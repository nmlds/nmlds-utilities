<#
    .SYNOPSIS
    Transfer any files in the root directory specified to the configured Google Cloud Storage bucket.
    Once files have successfully transferred, files are moved on the local file system to the transferred
    folder.
#>

param (
    # The full path to a service account key file.
    # This service account is recommended to only have the required permissions of storage.objects.list and storage.objects.create.
    [Parameter()]
    [string]
    $ServiceAccountKeyPath = "$PSScriptRoot\upload-svc-acct-key.json",

    # The folder to look for any files to upload. Does not recursively look in subdirectories at this path.
    [Parameter()]
    [string]
    $FilesToTransferFolderPath = "$PSScriptRoot\to-transfer",

    # The folder to move the file after successful upload.
    [Parameter()]
    [string]
    $FilesTransferredFolderPath = "$PSScriptRoot\transferred",

    # The Google Cloud Storage bucket the files should be uploaded to. For example: "rise-abc-d-gcs-landingzone-abcd". This should be the landing zone bucket in the desired environment.
    [Parameter(Mandatory=$true)]
    [string]
    $BucketName,

    # The Data Source System ID for the source of the data being uploaded. This is used to determine the path of the uploaded file and must match EXACTLY what was provided.
    [Parameter(Mandatory=$true)]
    [string]
    $DataSourceSystemId,

    # A comma-separated list of additional recipients of the ingestion summary for this execution
    [Parameter()]
    [string]
    $AdditionalEmailRecipients,

    # An override for the "path" in the GCS bucket where the files will land. This value should be coordinated align with what the backend service in GCP expects. This should only be speficied if an exceptional case arises of needing to upload to a different path. If specified, the $DataSourceSystemId parameter will be ignored.
    [Parameter()]
    [string]
    $ObjectPrefixOverride,

    # If true, the file name will have a timestamp (yyyyMMddHHmmss) appended just before the extension to avoid collisions. For example, myfile.csv would become myfile_20220901170101.csv.
    [Parameter()]
    [bool]
    $AppendTimestamp = $true,

    # The name of the `gcloud` configuration to activate and use. If one does not exist with this name, one will be created. In either case, a config value is set to use the private API endpoint instead of the public Internet API endpoint for Google Cloud Storage.
    [Parameter()]
    [string]
    $GcloudConfigName = "nmlds-ingestion",

    # The name of the Cloud Composer environment
    [Parameter()]
    [string]
    $CloudComposerEnvironmentName = "rise-rise-p-cc-393e",

    # The region of the Cloud Composer environment
    [Parameter()]
    [string]
    $CloudComposerRegion = "us-central1",

    # The project ID of the Cloud Composer environment
    [Parameter()]
    [string]
    $CloudComposerProjectId = "rise-nmlds-p-wh-ade2",

    # This enables the files to be sent across the public internet instead of using a private connection via VPN. Files will still be encrypted in transit. This switch also requires a user confirmation acknowledging that this file transfer will NOT use a private VPN connection.
    [Parameter()]
    [switch]
    $ForcePublicInternetTransit
)

$ErrorActionPreference = "Stop";

function Initialize-GcloudConfiguration {

    # Create if needed and activate `gcloud` CLI configuration
    $configs = (gcloud config configurations list --filter "name<=$GcloudConfigName AND name>=$GcloudConfigName" --format="json") | ConvertFrom-Json
    if ($configs.Count -eq 0) {
        gcloud config configurations create $GcloudConfigName
    }
    else {
        gcloud config configurations activate $GcloudConfigName
    }

    # If public transit flag not specified, set API endpoint override for GCS APIs to private endpoint
    if ($ForcePublicInternetTransit) {

        Write-Host -ForegroundColor Yellow -BackgroundColor Black "WARNING: The ForcePublicInternetTransit flag was specified. This enables the files to be sent across the public internet instead of using a private connection via VPN. Files will still be encrypted in transit, but by entering 'confirm' you are acknowledging that this file transfer will NOT use a private VPN connection."
        $confirm = Read-Host "(Enter 'confirm' to proceed, or anything else to cancel.)"
        if ($confirm -ne 'confirm') {
            exit
        }
        else {
            gcloud config unset api_endpoint_overrides/storage
        }
    }
    else {
        gcloud config set api_endpoint_overrides/storage "https://storage-nmlds.p.googleapis.com/storage/v1/"
    }

    # Disable Parallel Composite Upload -- this performance optimization feature that is enabled
    # by default on large files (by default ~150 MB) requires an additional "storage.objects.get" permission.
    # It is preferred to not require this permission for security reasons.
    # See more: https://cloud.google.com/storage/docs/parallel-composite-uploads
    gcloud config set storage/parallel_composite_upload_enabled False

    # Activate service account using key file specified
    gcloud auth activate-service-account --key-file=$ServiceAccountKeyPath
}

function Invoke-DataLakeSubmission {

    # Get list of files in root of input folder
    $files = Get-ChildItem $FilesToTransferFolderPath -File

    Write-Output "Found $($files.Count) files to process in to-transfer folder ($FilesToTransferFolderPath)..."

    $gcsUris = @()

    foreach ($file in $files) {
        if ($file -is [System.IO.FileInfo]) {
            try {
                $fileSourcePath = $file.FullName
                
                if ($AppendTimestamp) {
                    $fileNameWithoutExt = [System.IO.Path]::GetFileNameWithoutExtension($file.Name)
                    # Includes the "."
                    $fileExt = $file.Extension
                    
                    $objectFileName = "$($fileNameWithoutExt)_$(Get-Date -Format yyyyMMddHHmmss)$fileExt"
                    
                    if (-not ([string]::IsNullOrEmpty($ObjectPrefixOverride))) {
                        Write-Output "Using object prefix override specified..."
                        $objectKey = "$ObjectPrefixOverride/$objectFileName"
                    }
                    else {
                        $objectKey = "$DataSourceSystemId/unprocessed/$objectFileName"
                    }
                }
                else {
                    $objectKey = $file.Name
                }
                
                $gcsPath = "gs://$BucketName/$objectKey"
                
                Write-Output "COPYING file from $fileSourcePath to $gcsPath"

                gcloud storage cp $fileSourcePath $gcsPath 

                if ($LastExitCode -ne 0) {
                    Write-Error "Non-zero exit code from CLI during upload"
                }
                else {
                    $gcsUris += $gcsPath
                    $fileDestAfterTransfer = Join-Path -Path $FilesTransferredFolderPath -ChildPath $file.Name
                    Write-Output "Moving file from $fileSourcePath to $fileDestAfterTransfer"

                    # Finally, attempt to archive the local file to the specified destination to avoid reprocessing
                    try {
                        Move-Item -Path $fileSourcePath -Destination $fileDestAfterTransfer
                    }
                    catch {
                        Write-Host "Unable to move the file locally. File transfer still succeeded, but the file should be moved out of the to-transfer folder."
                    }
                }
            }
            catch {
                Write-Error "There was a problem transferring the file at $file"
                Write-Error $_.Exception.Message
            }
        }
    }

    $ingestionDagParams = @{
        "data_source"="$DataSourceSystemId";
        "gcs_files_to_ingest"="$($gcsUris -join ',')";
        "additional_email_recipients"="$AdditionalEmailRecipients"
    }
    $dagConfJson = $ingestionDagParams | ConvertTo-Json
    Write-Host "Triggering ingestion Airflow DAG with the following configuration: $dagConfJson"
    $composerCommand = "gcloud composer environments run $CloudComposerEnvironmentName --location $CloudComposerRegion --project $CloudComposerProjectId trigger_dag -- ingest_from_landingzone --conf '$dagConfJson'"
    Invoke-Expression -Command:$composerCommand
}

Initialize-GcloudConfiguration
Invoke-DataLakeSubmission
