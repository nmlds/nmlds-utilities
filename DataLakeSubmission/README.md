# Data Lake Submission Script

- [Data Lake Submission Script](#data-lake-submission-script)
  - [Prerequisites](#prerequisites)
  - [Required Parameters and Configuration](#required-parameters-and-configuration)
    - [Getting the service account key](#getting-the-service-account-key)
    - [Locating the storage bucket name](#locating-the-storage-bucket-name)
    - [Locating the `DataSourceSystemId` value](#locating-the-datasourcesystemid-value)
  - [DNS Configuration](#dns-configuration)
    - [A) Updating the server `hosts` file](#a-updating-the-server-hosts-file)
    - [B) Configuring on-prem DNS forwarding to Google Cloud DNS](#b-configuring-on-prem-dns-forwarding-to-google-cloud-dns)
  - [Getting Started](#getting-started)
  - [Service Account Permissions](#service-account-permissions)
  - [Additional Parameters](#additional-parameters)


A PowerShell script to be used for transferring flat files from a local file system to a Google Cloud Storage bucket for processing in the NMLDS data pipeline. The script scans a local folder for files, authenticates with a service account, and writes those files one by one into the bucket over VPN using the `gcloud` command line tool. Once the files are transferred successfully, the files are moved out of the local folder and into another local folder so they are not processed again.

Example:

    .\TransferFilesToDataLake.ps1 -BucketName rise-dept-d-gcs-landingzone-a1b2 -DataSourceSystemId mysystem -ServiceAccountKeyPath .\rise-dept-d-ing-a1b2-1234567890.json

The contents of this directory can be downloaded to a server with access to the files to be uploaded. The expectation is that uploads can be dropped into the included `to-transfer` and `transferred` folders and the included script can be executed on a scheduled basis. These folders include an empty file to ensure they are tracked in source control, but those files can be deleted once downloaded.

## Prerequisites

The following tools or constraints apply to this script:

- PowerShell installed and able to execute script
- `gcloud` must be installed and added on the PATH
- VPN connection available for private access to Google APIs
- DNS resolution for `storage-nmlds.p.googleapis.com` (see [DNS Configuration](#dns-configuration))
- Local folder created for files to transfer and a folder to move files into after transfer
  - If using script default settings, ensure that a `to-transfer` folder and a `transferred` folder exist in the same directory as the [TransferFilesToDataLake.ps1](TransferFilesToDataLake.ps1) script. Files intended to be uploaded should be placed in the `to-transfer` folder. They will be moved to the `transferred` folder upon completion.
- Service account key downloaded
  - If using script default settings, download the service account key for `sa-{DEPARTMENT}-data-lake-submission` and rename to `upload-svc-acct-key.json` in the same directory as the [TransferFilesToDataLake.ps1](TransferFilesToDataLake.ps1) script

## Required Parameters and Configuration

Each environment (sandbox, dev, test, prod) has a different service account and storage buckets.

For testing purposes, it is recommended to first retrieve the dev environment service account and uploading a file with no sensitive or production data.

The {ENV} token in the following naming examples is a single character abbreviation for the environment, as follows:
- s = sandbox
- d = dev
- t = test
- p = prod

**IMPORTANT: Production data should only be transferred directly to the `prod` environment.**

### Getting the service account key

In Google Cloud, each department using file upload should have one or more persons in the department's Ingestion Admins group. This group grants access to department-specific ingestion projects in Google Cloud. These projects are named as follows:

`rise-{DEPARTMENT}-{ENV}-ing-{XXXX}`

(Note: The following steps may vary slightly over time with UI changes in GCP.)

1. A user with membership in this group should log in to the [Google Cloud console](https://console.cloud.google.com).
2. Navigate to the [IAM Service Accounts](https://console.cloud.google.com/iam-admin/serviceaccounts) page.
3. Select the project in the options on the page or using the dropdown in the top navigation bar (e.g. `rise-{DEPARTMENT}-{ENV}-ing-{XXXX}`).
4. Click the service account listed named like `sa-{DEPARTMENT}-data-lake-submission`.
5. Click the `Keys` tab.
6. Click add a new key. Select `JSON` for the key type. Click create. This will initiate a download of the private portion of the key. **IMPORTANT: These credentials are sensitive and should be saved with care.**
7. Ensure that this private key is accessible by the script on the server it will be executed on. This path should be provided to the script at runtime as the `-ServiceAccountKeyPath` parameter.

### Locating the storage bucket name

Similar to the steps above for the service account key, each environment has its own Google Cloud Storage buckets for ingestion. In the `rise-{DEPARTMENT}-{ENV}-ing-{XXXX}` project, there is a bucket labeled `landingzone`. This full bucket name should be copied and supplied to the script's `-BucketName` parameter.

(Note: The bucket name, including the suffix, may vary across environments, so it is important to check in the console before running.)

1. A user with membership in this group should log in to the [Google Cloud console](https://console.cloud.google.com). **IMPORTANT: This should be done on a computer with access to the VPN.**
2. Navigate to the [Google Cloud Storage](https://console.cloud.google.com/storage/browser) page.
3. In the Buckets list, locate the bucket with `landingzone` in the name. This name should be copied and supplied to the script.

### Locating the `DataSourceSystemId` value

The `-DataSourceSystemId` parameter is an important identifier used throughout the ingestion to determine what ETL processes should be run for the files being uploaded. It is crucial the value supplied matches exactly what is documented (e.g. all lowercase letters, no extra spaces).

These identifiers can be found in the Technical Design Document for NMLDS or communication from the technical partners.

## DNS Configuration

The use of the VPN to transfer files requires that the address `storage-nmlds.p.googleapis.com` resolves correctly to the Private Service Connect IP address for the project. A couple of options for configuring this are listed below.

This IP address is omitted from this document, but it can be found in the Technical Design Documentation for NMLDS or communication from the technical partners along with additional information about DNS configuration.

### A) Updating the server `hosts` file

The quickest way to set up this resolution is to update the `hosts` file on the server running the script. An entry should be added that maps `storage-nmlds.p.googleapis.com` to the Private Service Connect IP. The primary downsides of this approach are that it requires system admin level access on any computer running this script and that IP address changes would require reconfiguring, although this IP address is expected to be stable.

### B) Configuring on-prem DNS forwarding to Google Cloud DNS

The more robust way to set up this resolution would be to configure the on-prem DNS server to forward DNS requests for `p.googleapis.com` to the Google Cloud DNS server. This is a more effective long-term solution, as this policy can be added to multiple computers and servers at the same time. It also means any IP address changes of the private endpoint's address can be picked up immediately by the on-prem servers.

## Getting Started

Once the [TransferFilesToDataLake.ps1](TransferFilesToDataLake.ps1) script is saved, the file directories are created and contain the desired files, and the service key is present, the script can be run. There are a number of default values for the parameters in the `.ps1` script, like the input/output file directories, service account key file path, and more. The script can be called with the desired configuration values like the following:

(In PowerShell)

    .\TransferFilesToDataLake.ps1 -BucketName rise-dept-d-gcs-landingzone-a1b2 -DataSourceSystemId mysystem -ServiceAccountKeyPath .\rise-dept-d-ing-a1b2-1234567890.json

Alternatively, the default values can be replaced in the `param` block of the script itself.

## Service Account Permissions

To reduce the risk unauthorized access of data, the service accounts used here only have the minimum permissions required associated with them. There is a custom role for this in the ingestion project in GCP where the service account is created. The role, `Data Lake Submitter`, only has `Storage Object List` and `Storage Object Create`, notably no `Storage Object Get` permission to retrieve the file once uploaded.

Credentials should still be handled with care, but if compromised, the user would not have the ability to download files that have already been uploaded.

## Additional Parameters

The PowerShell script contains some additional parameters which will likely not be needed, like specifying the name of the `gcloud` configuration, or forcing traffic to happen without the VPN for testing. For more information on these values, open the script, or run the following command:

    get-help .\TransferFilesToDataLake.ps1 -full

This script can be used as a reference for other scripting languages if the execution environment does not support running PowerShell scripts. If doing so, the most crucial pieces of the script are specifying the API endpoint override as "private" to ensure traffic occurs over the VPN and formatting the destination object path correctly.
